#pragma once


class Timer
{
public:
    Timer();
    double Update();

private:
    double m_deltaTime;
    double m_lastFrame;
};