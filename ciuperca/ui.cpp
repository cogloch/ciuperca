#include "ui.h"
#include "input_manager.h"
#include "util.h"

#include <fstream>
#include <string>


bool IsPointInsideRect(glm::vec2 point, glm::vec2 botLeft, glm::vec2 topRight)
{
    return (point.x >= botLeft.x && point.x <= topRight.x && point.y >= botLeft.y && point.y <= topRight.y);
}

UIContainer::UIContainer(unsigned int id, glm::vec2 relativePos, glm::vec2 relativeSize, UIContainer const * pParent)
    : elementType(NONE)
{
    this->id = id;

    this->relativePos = relativePos;
    this->relativeSize = relativeSize;

    pos = pParent->pos + glm::vec2(relativePos.x * pParent->size.x, relativePos.y * pParent->size.y);
    size = glm::vec2(relativeSize.x * pParent->size.x, relativeSize.y * pParent->size.y);
}

UIContainer::UIContainer(unsigned int id, glm::vec2 relativePos, glm::vec2 relativeSize, glm::vec2 parentPos, glm::vec2 parentSize)
    : elementId(NONE)
{
    this->id = id;

    this->relativePos = relativePos;
    this->relativeSize = relativeSize;

    pos = parentPos + glm::vec2(relativePos.x * parentSize.x, relativePos.y * parentSize.y);
    size = glm::vec2(relativeSize.x * parentSize.x, relativeSize.y * parentSize.y);
}

UISystem::UISystem(const std::string& layoutpath, glm::vec2 framebufSize)
{
    containers.reserve(64);

    LoadBaseLayout(layoutpath, framebufSize);
}

void UISystem::SetHoveredElement(glm::vec2 mousepos)
{
    hoveredElementId = -1;
    this->mousepos = mousepos;
    FindHoveredElement(0);
}

void UISystem::FindHoveredElement(int id)
{
    hoveredElementId = id;

    for (int childId : containers[id].children)
    {
        if (IsPointInsideRect(mousepos, containers[childId].pos, containers[childId].pos + containers[childId].size))
        {
            FindHoveredElement(childId);
            break;
        }
    }
}

void UISystem::ScaleContainer(int id, glm::vec2 factor)
{
    auto& cont = containers[id];

    cont.size.x *= factor.x;
    cont.size.y *= factor.y;

    for (auto& childId : cont.children)
    {
        ScaleContainer(childId, factor);
    }
}
// TODO: parsing error handling
void UISystem::LoadLayoutContainer(unsigned int parentId, rapidjson::Value& container)
{
    unsigned int id = AddContainer(parentId, { container["relativePos"][0].GetFloat(), container["relativePos"][1].GetFloat() }, 
                                             { container["relativeSize"][0].GetFloat(), container["relativeSize"][1].GetFloat() });

    if (container.FindMember("element") != container.MemberEnd())
    {
        std::string type = container["element"]["type"].GetString();
        if (type == "label")
        {
            auto fontpath = container["element"]["font"].GetString();
            auto text = container["element"]["text"].GetString();
            std::string colorText = container["element"]["color"].GetString();
            auto colorHex = std::stol(colorText, nullptr, 16);
            auto color = ColorHexToNorm(colorHex);
            AddLabel(id, fontpath, text, color);
        }
    }

    if(container.FindMember("children") != container.MemberEnd())
        for (rapidjson::SizeType i = 0; i < container["children"].Size(); i++)
            LoadLayoutContainer(id, container["children"][i]);
}

void UISystem::LoadBaseLayout(const std::string& layoutpath, glm::vec2 framebufSize)
{
    containers.clear();
    containers.reserve(64);
    containers.push_back(UIContainer(0, { 0.0f, 0.0f }, { 1.0f, 1.0f }, { 0.0f, 0.0f }, framebufSize));

    std::string line, strLayout;
    std::ifstream file(layoutpath);
    while (std::getline(file, line))
        strLayout += line + "\n";
    
    rapidjson::Document layout;
    layout.Parse(strLayout.c_str());

    rapidjson::Value& containers = layout["children"];
    for (rapidjson::SizeType i = 0; i < containers.Size(); i++)
        LoadLayoutContainer(0, containers[i]);
}

void UISystem::Tick()
{
    SetHoveredElement(InputManager::GetMousePos());
}

void UISystem::Scale(glm::vec2 factor)
{
    ScaleContainer(0, factor);
}

void UISystem::MoveContainer(int id, int parentId)
{
    containers[id].pos = containers[parentId].pos + glm::vec2(containers[parentId].size.x * containers[id].relativePos.x, containers[parentId].size.y * containers[id].relativePos.y);

    for (auto childId : containers[id].children)
        MoveContainer(childId, id);
}

void UISystem::OnScreenResize(glm::vec2 newSize)
{
    containers[0].size = newSize;

    for (auto childId : containers[0].children)
    {
        MoveContainer(childId, 0);
    }
}

void UISystem::AddLabel(unsigned int containerId, const std::string& fontpath, std::string text, glm::vec3 color)
{
    Label label;
    label.fontpath = fontpath;
    label.text = text;
    label.color = color;

    labels.push_back(label);

    containers[containerId].elementId = labels.size() - 1;
    containers[containerId].elementType = UIContainer::LABEL;
}

unsigned int UISystem::AddContainer(unsigned int parentId, glm::vec2 relativePos, glm::vec2 relativeSize)
{
    // TODO: Clamp within parent 

    UIContainer container(containers.size(), relativePos, relativeSize, &containers[parentId]);
    containers.push_back(container);
    containers[parentId].children.push_back(container.id);

    return container.id;
}

RenderState UISystem::GetRenderState(bool bRenderContainers)
{
    rstate.elems.clear();

    for (int containerId : containers[0].children)
        CookContainer(containerId, bRenderContainers);
        
    return rstate;
}

void UISystem::CookContainer(unsigned int containerId, bool bRenderContainerBg)
{
    auto& container = containers[containerId];

    if (bRenderContainerBg)
    {
        RenderElement rect;
        rect.type = RenderElement::RECT;

        if (containerId == hoveredElementId)
        {
            if (InputManager::IsMouseDown(InputManager::LEFT))
            {
                rect.color = { 1.0f, 0.0f, 0.0f };
            }
            else
            {
                rect.color = { 0.0f, 1.0f, 0.0f };
            }
        }
        else
        {
            rect.color = { 0.0f, 0.0f, 1.0f };
        }

        rect.x = container.pos.x;
        rect.y = container.pos.y;
        rect.width = container.size.x;
        rect.height = container.size.y;

        rstate.elems.push_back(rect);
    }

    switch(containers[containerId].elementType)
    { 
    case UIContainer::LABEL:
        rstate.elems.push_back(labels[container.elementId].GetRenderElement(&container));
        break;
    case UIContainer::BITMAP:
        rstate.elems.push_back(bitmaps[container.elementId].GetRenderElement(&container));
        break;
    }

    for (int childId : containers[containerId].children)
        CookContainer(childId, bRenderContainerBg);
}

RenderElement Label::GetRenderElement(UIContainer const * pContainer) const
{
    RenderElement elem;
    elem.type = RenderElement::TEXT;
    elem.color = color;
    elem.fontpath = fontpath;
    elem.text = text;

    elem.x = pContainer->pos.x;
    elem.y = pContainer->pos.y;
 
    elem.width = pContainer->size.x;
    elem.height = pContainer->size.y;
    return elem;
}

RenderElement Bitmap::GetRenderElement(UIContainer const * pContainer) const
{
    RenderElement elem;
    elem.type = RenderElement::SPRITE;

    return elem;
}
