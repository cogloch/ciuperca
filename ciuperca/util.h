#pragma once
#include <glm\glm.hpp>


constexpr glm::vec3 ColorRGBToNorm(float r, float g, float b)
{
    return{ r / 255.0f, g / 255.0f, b / 255.0f };
}

constexpr glm::vec3 ColorHexToNorm(int hexval)
{
    return{
        ((hexval >> 16) & 0xFF) / 255.0,
        ((hexval >> 8) & 0xFF) / 255.0,
        ((hexval) & 0xFF) / 255.0
    };
}