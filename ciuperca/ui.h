#pragma once
#include <glm\glm.hpp>
#include <vector>

#include "render_state.h"
#include "rapidjson/document.h"


struct UIContainer;

struct Label
{
    std::string fontpath;
    std::string text;
    glm::vec3 color;

    RenderElement GetRenderElement(UIContainer const * pContainer) const;
};

struct Bitmap
{

    RenderElement GetRenderElement(UIContainer const * pContainer) const;
};

// A rectangle used to position/size all UI elements 
// An UI element spans its entire container 
// Different from a grid in that it can have an absolute position independent of other cells/containers. May contain a grid. 
// TODO: make this accessible only to the UI system internally 
// TODO: move relative values outside, and into the UI system, separate from the actual containers 
struct UIContainer final
{
    UIContainer(unsigned int id, glm::vec2 relativePos, glm::vec2 relativeSize, UIContainer const * pParent);
    UIContainer(unsigned int id, glm::vec2 relativePos, glm::vec2 relativeSize, glm::vec2 parentPos, glm::vec2 parentSize);

    unsigned int id;

    glm::vec2 pos;          // Relative to screen
    glm::vec2 size;         // Relative to screen 

    glm::vec2 relativePos;  // Relative to parent
    glm::vec2 relativeSize; // Relative to parent 

    std::vector<unsigned int> children;

    unsigned int elementId;
    enum ElementType
    {
        NONE,
        LABEL, 
        BITMAP
    } elementType;
};

// TODO: Holds a state of the UI to be used when updating and a separate "view" of it, used for rendering
// TODO: floating windows
// TODO: reusable layouts 
struct UISystem
{
    UISystem(const std::string& layoutpath, glm::vec2 framebufSize);
    void LoadBaseLayout(const std::string& layoutpath, glm::vec2 framebufSize);
    
    void Tick();
    void OnScreenResize(glm::vec2 newSize);

    unsigned int AddContainer(unsigned int parentId, glm::vec2 relativePos, glm::vec2 relativeSize);
    void AddLabel(unsigned int containerId, const std::string& fontpath, std::string text, glm::vec3 color);

    // TODO: Cache render state and recreate only when the UI updates 
    RenderState GetRenderState(bool bRenderContainers);
    void CookContainer(unsigned int containerId, bool bRenderContainerBg);

    RenderState rstate;

    std::vector<Label> labels;
    std::vector<Bitmap> bitmaps;

    // Only one element at any time may be hovered or being pressed, so it doesn't make sense to track the state for each element in part 
    int hoveredElementId;  
    glm::vec2 mousepos;
    
    // containers[0] is always the base screen container - defined by a loadable layout, spans the entire screen and can't be dragged around 
    std::vector<UIContainer> containers;

private:
    void LoadLayoutContainer(unsigned int parentId, rapidjson::Value&);

    // This is retardedly shit 
    void SetHoveredElement(glm::vec2 mousepos);
    void FindHoveredElement(int id);
    
    void Scale(glm::vec2 factor);
    void ScaleContainer(int id, glm::vec2 factor);
    void MoveContainer(int id, int parentId);
};