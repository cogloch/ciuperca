#pragma once
#include <glm\glm.hpp>
#include <vector>
#include <algorithm>


struct RenderElement
{
    enum Type
    {
        UNKNOWN, RECT, SPRITE, TEXT
    } type;

    float x, y, width, height;
    glm::vec3 color;
    std::string texturepath;
    std::string fontpath;
    std::string text;
};

struct RenderState
{
    std::vector<RenderElement> elems;
    void SortByType()
    {
        std::stable_sort(elems.begin(), elems.end(), [](const RenderElement& a, const RenderElement& b) -> bool { return a.type < b.type; });
    }
};