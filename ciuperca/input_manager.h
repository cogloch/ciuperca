#pragma once
#include <glm\glm.hpp>


struct GLFWwindow;

class InputManager
{
public:
    static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
    static void MouseCallback(GLFWwindow* window, double xpos, double ypos);
    static void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
    
    static bool IsKeyDown(int key) { return keys[key]; }
    static glm::vec2 GetMousePos() { return mouseLastPos; }
    static glm::vec2 GetMousDeltaPos() { return mouseDeltaPos; }
    static double GetMouseScrollDelta() { return mouseScrollDeltaY; }

    enum MouseButton
    {
        LEFT, MIDDLE, RIGHT
    };

    static void SetMouseState(MouseButton button, bool bDown) { mouseState[button] = bDown; }
    static bool IsMouseDown(MouseButton button) { return mouseState[button]; }

    static void SetFramebufHeight(double height) { framebufHeight = height; }

private:
    static bool bFirstMouse;
    static glm::vec2 mouseLastPos;
    static glm::vec2 mouseDeltaPos;
    static double mouseScrollDeltaY;

    static bool mouseState[3];

    static double framebufHeight; // Used to invert y pos of cursor 
    
    static bool keys[1024];
};