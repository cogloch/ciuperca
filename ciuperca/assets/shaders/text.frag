#version 330 core

in vec2 TexCoord;

out vec4 color;

uniform sampler2D tex;
uniform vec3 modColor;

void main()
{
	color = vec4(modColor, 1.0f) * vec4(1.0, 1.0, 1.0, texture(tex, TexCoord).r);
}