#pragma once
#include <string>
#include <map>

#include <GL\glew.h>
#include <glm\glm.hpp>
#include "stb_image.h"


// Fonts
struct Character {
    GLuint     textureID;  // ID handle of the glyph texture
    glm::ivec2 size;       // Size of glyph
    glm::ivec2 bearing;    // Offset from baseline to left/top of glyph
    GLuint     advance;    // Offset to advance to next glyph
};

struct Font
{
    std::map<GLchar, Character> characters;
};

class ResourceManager
{
public:
    void LoadTexture(const char* path, GLint format);
    void LoadShader(const std::string& path);

    void LoadFont(const std::string& path);

    std::map<std::string, GLuint> textures;
    std::map<std::string, GLuint> shaders;
    
    std::map<std::string, Font> fonts;
};