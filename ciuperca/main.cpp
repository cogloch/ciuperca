#include "game.h"


#pragma comment(lib, "glfw3.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32s.lib")
#pragma comment(lib, "freetype27.lib")


int main()
{
    GameConfig config = { 800, 600, 3, 3 };
    Game game(config);
    game.Run();
}