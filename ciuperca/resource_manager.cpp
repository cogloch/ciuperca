#include "resource_manager.h"
#include "shader.h"

#include <fstream>
#include <iterator>
#include <iostream>

#include <ft2build.h>
#include FT_FREETYPE_H


void ResourceManager::LoadTexture(const char* path, GLint format)
{
    GLuint texture;
    glGenTextures(1, &texture);

    glBindTexture(GL_TEXTURE_2D, texture);

    int width, height, n;
    unsigned char* image = stbi_load(path, &width, &height, &n, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);
    stbi_image_free(image);

    glBindTexture(GL_TEXTURE_2D, 0);

    textures[path] = texture;
}

void ResourceManager::LoadShader(const std::string& path)
{
    std::ifstream vertShaderFile(path + ".vert");
    std::string vertSource(std::istreambuf_iterator<char>{vertShaderFile}, {});

    std::ifstream fragShaderFile(path + ".frag");
    std::string fragSource(std::istreambuf_iterator<char>{fragShaderFile}, {});

    shaders[path] = CreateShaderProgram(vertSource.c_str(), fragSource.c_str());
}

void ResourceManager::LoadFont(const std::string& path)
{
    Font font;

    FT_Library ft;
    if (FT_Init_FreeType(&ft))
        throw "Failed to initialize FT lib";

    FT_Face face;
    if (FT_New_Face(ft, path.c_str(), 0, &face))
        throw "Failed to load font";

    // Font size to extract from face
    // Set width to 0 to dynamically calculate the width based on the given height 
    FT_Set_Pixel_Sizes(face, 0, 48);

    // Create an 8bit grayscale bitmap 
    if (FT_Load_Char(face, 'X', FT_LOAD_RENDER))
        throw "Failed to load glyph";

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

    for (GLubyte c = 0; c < 128; c++)
    {
        // Load character glyph 
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
        }
        // Generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer
        );
        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // Now store character for later use
        Character character = {
            texture,
            glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            face->glyph->advance.x
        };
        font.characters.insert(std::pair<GLchar, Character>(c, character));
    }

    FT_Done_Face(face);
    FT_Done_FreeType(ft);

    fonts[path] = font;
}