#include "timer.h"
#include <GLFW\glfw3.h>


Timer::Timer()
    : m_deltaTime(0.0)
    , m_lastFrame(0.0)
{
}

double Timer::Update()
{
    double currentFrame = glfwGetTime();
    m_deltaTime = currentFrame - m_lastFrame;
    m_lastFrame = currentFrame;
    
    return m_deltaTime;
}
