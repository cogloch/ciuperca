#include "game.h"

#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <string>

#include "util.h"

// Systems
#include "ui.h"
#include "renderer.h"
#include "input_manager.h"


int currentFramebufWidth, currentFramebufHeight;
UISystem* pUI;
Renderer* pRenderer;

Game::Game(GameConfig& config)
    : m_config(config)
    , m_pWindow(nullptr)
    , m_camera(glm::vec3(0.0f, 0.0f, 3.0f))
{
    InitWindow();

    pUI = new UISystem("assets/ui_layouts/test.json", { currentFramebufWidth, currentFramebufHeight });
    pRenderer = new Renderer(currentFramebufWidth, currentFramebufHeight);
    InputManager::SetFramebufHeight(currentFramebufHeight);
}

Game::~Game()
{
    glfwTerminate();
}

void OnFramebufScale(GLFWwindow* pWindow, int width, int height)
{
    glm::vec2 scaleFactor;

    scaleFactor.x = (float)width / (float)currentFramebufWidth;
    scaleFactor.y = (float)height / (float)currentFramebufHeight;

    currentFramebufWidth = width;
    currentFramebufHeight = height;

    pRenderer->ScaleFramebuf(width, height);
    InputManager::SetFramebufHeight(height);
    
    // Reposition elements instead of scaling them
    // Also most likely don't scale 1:1 when doing ui scaling(scaling curve?)
    pUI->OnScreenResize({ width, height });
}

void Game::InitWindow()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, m_config.glVerMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, m_config.glVerMinor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // TODO moooore options :-)
    glfwWindowHint(GLFW_SAMPLES, 4);
    
    // Window resizing 
    //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // TODO fullscreen/borderless support 
    m_pWindow = glfwCreateWindow(m_config.windowWidth, m_config.windowHeight, "abenakiremovalsimulator2017.jpg", nullptr, nullptr);
    if (!m_pWindow)
    {
        glfwTerminate();
        throw "Failed to create Opengl window\n";
    }
    glfwMakeContextCurrent(m_pWindow);

    glfwGetFramebufferSize(m_pWindow, &currentFramebufWidth, &currentFramebufHeight);
    glfwSetFramebufferSizeCallback(m_pWindow, OnFramebufScale);
    glfwSetWindowAspectRatio(m_pWindow, 4, 3);

    // GLFW callbacks
    glfwSetKeyCallback(m_pWindow, InputManager::KeyCallback);

    //glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(m_pWindow, InputManager::MouseCallback);

    glfwSetScrollCallback(m_pWindow, InputManager::ScrollCallback);
}

void Game::Run()
{
    while (!glfwWindowShouldClose(m_pWindow))
    {
        double dt = m_timer.Update();

        glfwPollEvents();

        Tick(dt);

        InputManager::SetMouseState(InputManager::LEFT, glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_LEFT));
        InputManager::SetMouseState(InputManager::MIDDLE, glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_MIDDLE));
        InputManager::SetMouseState(InputManager::RIGHT, glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_RIGHT));
        
        RenderState rstate;

        pUI->Tick();
        
        auto rstateui = pUI->GetRenderState(true);
        rstate.elems.insert(
            rstate.elems.end(),
            std::make_move_iterator(rstateui.elems.begin()),
            std::make_move_iterator(rstateui.elems.end())
        );

        pRenderer->Tick(m_camera, dt, rstate);
        glfwSwapBuffers(m_pWindow);
    }
}

void Game::Tick(double dt)
{
    m_camera.Tick(dt);
}
