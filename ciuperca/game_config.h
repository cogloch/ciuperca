#pragma once


struct GameConfig
{
    int windowWidth, windowHeight;
    int glVerMajor, glVerMinor;
};