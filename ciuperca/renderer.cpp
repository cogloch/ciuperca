#include "renderer.h"

#define GLEW_STATIC
#include <GL\glew.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "resource_manager.h"
#include <array>

#include "render_state.h"


std::array<float, 4 * 6> GenerateQuad(float xpos, float ypos, float w, float h)
{
    return {
        xpos,     ypos + h,   0.0, 0.0,
        xpos,     ypos,       0.0, 1.0,
        xpos + w, ypos,       1.0, 1.0,

        xpos,     ypos + h,   0.0, 0.0,
        xpos + w, ypos,       1.0, 1.0,
        xpos + w, ypos + h,   1.0, 0.0
    };
}

ResourceManager resources;
GLuint spriteShaderId, textShaderId, colorRectShaderId;
GLint spriteShaderProjectionLoc, textShaderProjectionLoc; 
GLint spriteShaderModColorLoc, textShaderModColorLoc;
GLint colorRectShaderProjectionLoc, colorRectShaderColorLoc;
glm::mat4 projection;
GLuint quadvao;


Renderer::Renderer(int framebufferWidth, int framebufferHeight)
    : m_framebufferWidth(framebufferWidth)
    , m_framebufferHeight(framebufferHeight)
{
    InitGL();
    InitResources();
}

void RenderRect(glm::vec3 color, float xpos, float ypos, float width, float height)
{
    glUniform3fv(colorRectShaderColorLoc, 1, &color[0]);

    glBindVertexArray(quadvao);
    auto verts = GenerateQuad(xpos, ypos, width, height);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(verts), verts.data());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

void RenderSprite(const std::string& texturePath, glm::vec3 color, float xpos, float ypos, float width, float height)
{
    glBindTexture(GL_TEXTURE_2D, resources.textures[texturePath]);
    glUniform3fv(spriteShaderModColorLoc, 1, &color[0]);

    glBindVertexArray(quadvao);
    auto verts = GenerateQuad(xpos, ypos, width, height);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(verts), verts.data());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

void RenderText(const std::string& font, std::string text, GLfloat x, GLfloat y, glm::vec2 containerSize, glm::vec3 color)
{
    // Calculate scaling
    glm::vec2 scale;
    glm::vec2 unscaledSize = { 0, 0 };
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = resources.fonts[font].characters[*c];
        // TODO: fix total width calculation 
        unscaledSize.x += ch.size.x;// + (ch.advance >> 6);

        if (ch.size.y > unscaledSize.y)
            unscaledSize.y = ch.size.y;
    }

    scale.x = containerSize.x / unscaledSize.x;
    scale.y = containerSize.y / unscaledSize.y;

    // Activate corresponding render state	
    glUniform3fv(textShaderModColorLoc, 1, &color[0]);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(quadvao);

    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = resources.fonts[font].characters[*c];

        GLfloat xpos = x + ch.bearing.x * scale.x;
        GLfloat ypos = y - (ch.size.y - ch.bearing.y) * scale.y;

        GLfloat w = ch.size.x * scale.x;
        GLfloat h = ch.size.y * scale.y;
        // Update VBO for each character
        GLfloat vertices[6][4] = {
            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }
        };
        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.textureID);
        // Update content of VBO memory
        //glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        //glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.advance >> 6) * scale.x; // Bitshift by 6 to get value in pixels (2^6 = 64)
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::Tick(Camera& camera, float dt, RenderState& state)
{
    //glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (state.elems.empty()) return;

    state.SortByType();
    RenderElement::Type curType = RenderElement::Type::UNKNOWN;
    for (auto& elem : state.elems)
    {
        if (elem.type != curType)
        {
            curType = elem.type;

            switch (elem.type)
            {
            case RenderElement::RECT:
                glUseProgram(colorRectShaderId);
                glUniformMatrix4fv(colorRectShaderProjectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
                break;
            case RenderElement::SPRITE:
                glUseProgram(spriteShaderId);
                glUniformMatrix4fv(spriteShaderProjectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
                break;
            case RenderElement::TEXT:
                glUseProgram(textShaderId);
                glUniformMatrix4fv(textShaderProjectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
                break;
            }
        }

        switch (elem.type)
        {
        case RenderElement::RECT:
            RenderRect(elem.color, elem.x, elem.y, elem.width, elem.height);
            break;
        case RenderElement::SPRITE:
            RenderSprite(elem.texturepath, elem.color, elem.x, elem.y, elem.width, elem.height);
            break;
        case RenderElement::TEXT:
            RenderText(elem.fontpath, elem.text, elem.x, elem.y, { elem.width, elem.height }, elem.color);
            break;
        }
    }
}

void Renderer::ScaleFramebuf(float newWidth, float newHeight)
{
    m_framebufferWidth = newWidth;
    m_framebufferHeight = newHeight;
    glViewport(0, 0, newWidth, newHeight);
    projection = glm::ortho(0.0f, (float)m_framebufferWidth, 0.0f, (float)m_framebufferHeight);
}

void Renderer::InitGL()
{
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        throw "Failed to initialize GLEW\n";
    }

    // Actual framebuffer size may different from window size
    glViewport(0, 0, m_framebufferWidth, m_framebufferHeight);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glEnable(GL_MULTISAMPLE);
}

GLuint GenQuadVAO()
{
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, nullptr, GL_DYNAMIC_DRAW);

    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
    return vao;
}

void Renderer::InitResources()
{
    quadvao = GenQuadVAO();

    resources.LoadTexture("assets/textures/wall.jpg", GL_RGB);
    resources.LoadTexture("assets/textures/awesomeface.png", GL_RGBA);

    resources.LoadShader("assets/shaders/sprite");
    resources.LoadShader("assets/shaders/text");
    resources.LoadShader("assets/shaders/colorRect");

    spriteShaderId = resources.shaders["assets/shaders/sprite"];
    spriteShaderProjectionLoc = glGetUniformLocation(spriteShaderId, "projection");
    spriteShaderModColorLoc = glGetUniformLocation(spriteShaderId, "modColor");
    
    textShaderId = resources.shaders["assets/shaders/text"];
    textShaderProjectionLoc = glGetUniformLocation(textShaderId, "projection");
    textShaderModColorLoc = glGetUniformLocation(textShaderId, "modColor");

    colorRectShaderId = resources.shaders["assets/shaders/colorRect"];
    colorRectShaderProjectionLoc = glGetUniformLocation(colorRectShaderId, "projection");
    colorRectShaderColorLoc = glGetUniformLocation(colorRectShaderId, "Color");

    projection = glm::ortho(0.0f, (float)m_framebufferWidth, 0.0f, (float)m_framebufferHeight);

    resources.LoadFont("assets/fonts/Pacifico.ttf");
    resources.LoadFont("assets/fonts/VeraMono.ttf");
}
