#include "input_manager.h"
#include <GLFW\glfw3.h>


bool InputManager::bFirstMouse = true;
glm::vec2 InputManager::mouseLastPos;
glm::vec2 InputManager::mouseDeltaPos;
double InputManager::mouseScrollDeltaY;

bool InputManager::mouseState[3] = { false, false, false };

double InputManager::framebufHeight;

bool InputManager::keys[1024];

void InputManager::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if (key >= 0 && key < 1024)
    {
        if (action == GLFW_PRESS)
            keys[key] = true;
        else if (action == GLFW_RELEASE)
            keys[key] = false;
    }
}

void InputManager::MouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    // Origin at bottom left 
    ypos = framebufHeight - ypos;

    if (bFirstMouse)
    {
        mouseLastPos = { xpos, ypos };
        bFirstMouse = false;
    }

    mouseDeltaPos = {
        xpos - mouseLastPos.x,
        mouseLastPos.y - ypos // Reversed since origin is at bottom
    };

    mouseLastPos = { xpos, ypos };
}

void InputManager::ScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    mouseScrollDeltaY = yoffset;
}
