#pragma once
#include "camera.h"
#include <vector>
#include <algorithm>


struct RenderState;

class Renderer
{
public:
    Renderer(int framebufferWidth, int framebufferHeight);
    void Tick(Camera& camera, float dt, RenderState& state);

    void ScaleFramebuf(float newWidth, float newHeight);

private:
    int m_framebufferWidth, m_framebufferHeight;

private:
    void InitGL();
    void InitResources();
};