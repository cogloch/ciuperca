#pragma once


// A scenario defines the map/world, climate and available colonialist nations 
// Things may get duplicated between scenarios, like France being a colonail nation in the Caribbean and New England;
// this is allowed given that scenarios are meant to not have dependencies, and can stand on their own to be shared,
// bought, etc. 
struct GameScenario
{

};