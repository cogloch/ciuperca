#pragma once


GLuint CompileShader(GLenum type, const char* source);

GLuint LinkShaderProgram(GLuint vertexShader, GLuint fragmentShader);

GLuint CreateShaderProgram(const char* vertexShaderSource, const char* fragmentShaderSource);