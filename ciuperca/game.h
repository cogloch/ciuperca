#pragma once
#include "game_config.h"
#include "camera.h"
#include "timer.h"


struct GLFWwindow;

class Game
{
public:
    Game(GameConfig& config);
    ~Game();
    void Run();

private:
    GameConfig m_config;
    GLFWwindow* m_pWindow;
    Camera m_camera;
    Timer m_timer;

private:
    void InitWindow();
    void Tick(double dt);
};